import json
import requests
import jsonpath

with open("URL.json", 'r') as json_file:
    URL = json.load(json_file)

def test_get_request():
    response = requests.get(URL['GET'])
    assert response.status_code == 200
    print("Asserted Status code:", response.status_code)


def test_post_request():
    #Read Input JSON File
    file = open('POST.json', 'r')
    json_input = file.read()
    post_arguments = json.loads(json_input)
    file.close()
    response = requests.post(URL['POST'], post_arguments)
    print("Post Response content:", response.content)
    assert response.status_code == 201
    print("Asserted Status code:", response.status_code)
    #Parse Response to JSON
    response_json = json.loads(response.text)
    with open('POST_RESPONSE.json', 'a') as outputfile:
        json.dump(response_json, outputfile)
    Id = jsonpath.jsonpath(response_json, 'id')
    print(Id[0])


def test_put_request():
    #Read Input Json
    file = open('PUT.json', 'r')
    json_input = file.read()
    request_json = json.loads(json_input)
    file.close()
    response = requests.put(URL['PUT'], request_json)
    print("Put Response content:", response.content)
    assert response.status_code == 200
    print("Asserted Status code:", response.status_code)
    response_json = json.loads(response.text)
    with open('PUT_RESPONSE.json', 'a') as file_put:
        json.dump(response_json, file_put)
    updatedAt = jsonpath.jsonpath(response_json, 'updatedAt')
    print("UpdatedAt",updatedAt)

def test_delete_request():

    response = requests.delete(URL['DELETE'])
    assert response.status_code == 204
    print("Asserted Status code:",response.status_code)
